﻿using System;

namespace HepsiBuradaExample.Services.Helpers
{
    public static class ServiceMessageHelper
    {
        public static string GetExceptionMessage(ErrorType errorType, DataType? dataType = null)
        {
            var messageText = errorType switch
            {
                ErrorType.NotFound => $"{dataType} not found!",
                ErrorType.UnexpectedError => "An unexpected error was encountered during the operation.",
                _ => throw new NotImplementedException()
            };

            return messageText;
        }
    }

    public enum ErrorType
    {
        UnexpectedError,
        NotFound
    }

    public enum DataType
    {
        Product,
        Order,
        Campaign
    }
}
