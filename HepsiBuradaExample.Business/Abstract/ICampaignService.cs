﻿using HepsiBuradaExample.Services.Dtos.Campaign;

namespace HepsiBuradaExample.Services.Abstract
{
    public interface ICampaignService
    {
        CampaignDTO GetCampaignByName(string campaignName);
        CampaignDTO CreateCampaign(CampaignDTO campaign);
        CampaignDTO EndCampaignsByEndDate();
        CampaignDetailDTO GetCampaignDetailByName(string campaignName);
    }
}
