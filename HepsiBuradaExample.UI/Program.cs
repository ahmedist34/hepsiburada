﻿using HepsiBuradaExample.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HepsiBuradaExample.UI
{
    public class Program
    {
        static void Main(string[] args)
        {
            
            #region Configure Services
            Startup.ConfigureServices();
            
            #endregion
            var command = string.Empty;
            do
            {
                Console.WriteLine("Please Enter Your Action: ");
                var commandLine = Console.ReadLine();

                if (commandLine != null)
                {
                    var commandArray = commandLine.Split(null).Where(x => x != "").ToArray();

                    if (commandArray.Any())
                    {
                        command = commandArray[0];
                        var hasCommand = DispatcherEngine.CommandSet.TryGetValue(command, out var @delegate);

                        if (hasCommand)
                        {
                            var parameters = new List<object>();

                            for (var i = 1; i < commandArray.Length; i++)
                                parameters.Add(commandArray[i]);

                            try
                            {
                                dynamic result = @delegate.DynamicInvoke(parameters.ToArray());

                                if (result != null)
                                {
                                    string objectToInstantiate = result.GetType().AssemblyQualifiedName;
                                    var objectType = Type.GetType(objectToInstantiate);
                                    dynamic instantiatedObject = Activator.CreateInstance(objectType) as IInfoModel;
                                    instantiatedObject = result;

                                    Console.WriteLine(instantiatedObject.GetInfo());
                                }
                            }
                            catch
                            {
                                var helpText = string.Empty;
                                DispatcherEngine.HelpMenu.TryGetValue(command, out helpText);
                                Console.WriteLine("Incorrect command usage!");
                                Console.WriteLine(helpText);
                            }
                        }
                        else
                            Console.WriteLine("Command not found!");               
                    }
                }
            } while (command != "exit");

            Startup.DisposeServices();

        }
    }
}