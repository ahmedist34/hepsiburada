﻿using HepsiBuradaExample.Data.Abstract;
using HepsiBuradaExample.Data.Entities;
using HepsiBuradaExample.Data.Concrete.EntityFramework.Base;

namespace HepsiBuradaExample.Data.Concrete.EntityFramework
{
    public class EfCampaignRepository : EfBaseRepository<Campaign>, ICampaignRepository
    {
        public EfCampaignRepository(HepsiBuradaExampleContext context) : base(context)
        {

        }


    }
    
}
