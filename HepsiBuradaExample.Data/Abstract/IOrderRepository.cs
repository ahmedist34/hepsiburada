﻿using HepsiBuradaExample.Data.Abstract.Base;
using HepsiBuradaExample.Data.Entities;

namespace HepsiBuradaExample.Data.Abstract
{
    public interface IOrderRepository : IBaseRepository<Order>
    {

    }
}
